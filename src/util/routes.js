import Overview from "../components/views/Overview.vue";
import Login from "../components/views/Login.vue";

import IndexOfUniversities from "../components/views/universities/IndexOfUniversities.vue";
import NewUniversity from "../components/views/universities/NewUniversity.vue";
import ShowUniversity from "../components/views/universities/ShowUniversity.vue";
import EditUniversity from "../components/views/universities/EditUniversity.vue";

import IndexofCompanies from "../components/views/companies/IndexOfCompanies.vue";
import NewCompany from "../components/views/companies/NewCompany.vue";
import ShowCompany from "../components/views/companies/ShowCompany.vue";
import EditCompany from "../components/views/companies/EditCompany.vue";

import NewCooperation from "../components/views/cooperation/NewCooperation.vue";
import EditCooperation from "../components/views/cooperation/EditCooperation.vue";

export default [

    { path: "/", component: Overview, name: "home" },

    { path: "/login", component: Login, name: "login" },

    //Universities routes
    { path: "/uczelnie", component: IndexOfUniversities, name: "index_of_universities"},
    { path: "/uczelnie/new", component: NewUniversity, name: "new_university"},
    { path: "/uczelnie/:id", component: ShowUniversity, name: "show_university"},
    { path: "/uczelnie/:id/edit", component: EditUniversity, name: "edit_university"},

    //Companies routes
    { path: "/firmy", component: IndexofCompanies, name: "index_of_companies"},
    { path: "/firmy/new", component: NewCompany, name: "new_company"},
    { path: "/firmy/:id", component: ShowCompany, name: "show_company"},
    { path: "/firmy/:id/edit", component: EditCompany, name: "edit_company"},

    //Cooperation routes
    { path: "/wspolpraca/new", component: NewCooperation, name: "new_cooperation"},
    { path: "/wspolpraca/:universityId/:companyId/edit", component: EditCooperation, name: "edit_cooperation"}
]
