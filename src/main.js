import "./style.scss";

import Vue from "vue";

import VueResource from "vue-resource";
Vue.use(VueResource);

import VueRouter from 'vue-router';
Vue.use(VueRouter);
import routes from './util/routes';
const router = new VueRouter({ routes });

import store from "./store";

import Container from "./components/Container.vue";
import NavBar from "./components/NavBar.vue";

new Vue({
    el: "#app",
    data: {
        companies:[],
        universities:[],
        cart: "home"
    },
    components: {
        Container,
        NavBar
    },
    store,
    methods: {
        getCompanies: function() {
            this.companies = [];
            return this.$http
                .get('/firmy')
                .then(response => {
                    this.companies = response.data;
                }, response => {
                    console.log("error");
            });
        },
        getUniversities: function() {
            this.universities = [];
             return this.$http
                .get('/uczelnie')
                .then(response => {
                    this.universities = response.data;
                }, response => {
                    console.log("error");
            });
        },
        setCart(payload) {
            this.cart = payload;
        }
    },
    created() {
        Promise.all([this.getCompanies(),this.getUniversities()]).then(() => {
            if(window.__INITIAL_STATE__.currentUser) {
                this.$store.commit("setCurrentUser", window.__INITIAL_STATE__.currentUser)
            } else {
                this.$store.commit("setCurrentUser", false);
            }
        })
    },
    router
})
