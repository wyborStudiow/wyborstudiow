var mongoose = require("mongoose");

var universitySchema = new mongoose.Schema({
   name: String,
   img: String,
   description: String,
   companies: [{
      item:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Company"
         },
      zasadyWspolpracy: String,
      ocenaRedakcji: Number
   }]
});

module.exports = mongoose.model("University", universitySchema);
