var mongoose = require("mongoose");

var companySchema = new mongoose.Schema({
   name: String,
   img: String,
   description: String,
   universities: [{
      item:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "University"
         },
      zasadyWspolpracy: String,
      ocenaRedakcji: Number
  }],
});

module.exports = mongoose.model("Company", companySchema);
