var express = require("express");
var router  = express.Router();
var passport = require("passport");
var User = require("../models/user");
const fs = require('fs');
const path = require('path');
const serialize = require("serialize-javascript");

//root route
router.get("/", function(req, res){
    let template = fs.readFileSync(path.resolve('./index.html'), 'utf-8');
    let contentMarker = "<!--APP-->";
    let initialState = {
        currentUser: res.locals.currentUser
    }
    console.log("Przesylam template");
    res.send(template.replace(contentMarker, `<script>var __INITIAL_STATE__ = ${serialize(initialState)}</script>`));
});

//handling login logic
router.post("/login", passport.authenticate("local",
    {
        successRedirect: "/#/",
        failureRedirect: "/#/login"
    }), function(req, res){
});

// logout route
router.get("/logout", function(req, res){
   req.logout();
   console.log("wylogowano");
   //req.flash("success", "LOGGED YOU OUT!");
   res.redirect("/#/");
});

module.exports = router;
