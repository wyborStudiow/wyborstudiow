var express     = require("express");
var router      = express.Router({mergeParams: true});
var University  = require("../models/university");
var Company     = require("../models/company");
var middleware = require("../middleware");
var request     = require("request");


router.get("/", function(req, res){
    res.send("Dodales nowa wspoprace");
});

router.post("/", middleware.isLoggedIn, function(req, res){
   University.findById(req.body.idUczelni, function(err, university){
       if(err){
           console.log(err);
           res.redirect("/universities");
       } else {
        Company.findById(req.body.idFirmy, function(err, company){
           if(err){
               console.log(err);
           } else {
               var zasadyWspolpracyFirmy = req.body.zasadyWspolpracy;
               var ocenaRedakcji = req.body.ocenaRedakcji;
               university.companies.push({item: company, zasadyWspolpracy: zasadyWspolpracyFirmy, ocenaRedakcji: ocenaRedakcji});
               university.save();
               company.universities.push({item: university, zasadyWspolpracy: zasadyWspolpracyFirmy, ocenaRedakcji: ocenaRedakcji});
               company.save();
               res.redirect('/#/uczelnie/' + university.id);
           }
        });
       }
   });
});

router.get("/:universityId/:companyId/edit", middleware.isLoggedIn, function(req, res){
    Company.findById(req.params.companyId, function(err, company) {
        if(err) {
            console.log(err);
        } else {
            University.findById(req.params.universityId).populate("companies.item").exec(function(err, uni) {
                if(err) {
                    console.log(err);
                } else {
                    uni.companies.forEach(function (foundCompany) {
                       if(foundCompany.item.id === req.params.companyId) {
                           console.log("In EDIT");
                            return res.send({university: uni, company: company, zasadyWspolpracy: foundCompany.zasadyWspolpracy, ocenaRedakcji: foundCompany.ocenaRedakcji});
                       }
                    });
                }
            });
        }
    });
});

router.put("/:universityId/:companyId", middleware.isLoggedIn, function(req, res){
   console.log("poszla metoda put");
   University.findById(req.params.universityId).populate("companies.item").exec(function(err, university){
        if(err) {
            console.log(err);
            res.redirect("/#/universities");
        } else {
            for(var i = 0; i < university.companies.length; i++) {
                if(university.companies[i].item.id === req.params.companyId) {
                    university.companies[i].zasadyWspolpracy = req.body.zasadyWspolpracy;
                    university.companies[i].ocenaRedakcji = req.body.ocenaRedakcji;
                    break;
                }
            }
            Company.findById(req.params.companyId).populate("universities.item").exec(function(err, company){
               if(err) {
                   console.log(err);
                   res.redirect("/#/universities");
               } else {
                   console.log(req.params.companyId);
                   console.log(company);
                    for(var i = 0; i < company.universities.length; i++) {
                        if(company.universities[i].item.id === req.params.universityId) {
                            company.universities[i].zasadyWspolpracy = req.body.zasadyWspolpracy;
                            company.universities[i].ocenaRedakcji = req.body.ocenaRedakcji;
                            break;
                        }
                    }
                    university.save();
                    company.save();
                    res.redirect('/#/uczelnie/' + university.id);
                }
            });
        }
   });
});

router.delete("/:universityId/:companyId", middleware.isLoggedIn, function(req, res) {
    University.findById(req.params.universityId).populate("companies.item").exec(function(err, university){
       if(err) {
           console.log(err);
           res.redirect("/#/universities");
       } else {
            for(var i = 0; i < university.companies.length; i++) {
                if(university.companies[i].item.id === req.params.companyId) {
                    university.companies.splice(i, 1);
                    break;
                }
            }
            Company.findById(req.params.companyId).populate("universities.item").exec(function(err, company){
               if(err) {
                   console.log(err);
                   res.redirect("/#/universities");
               } else {
                   console.log(req.params.companyId);
                   console.log(company);
                    for(var i = 0; i < company.universities.length; i++) {
                        if(company.universities[i].item.id === req.params.universityId) {
                            company.universities.splice(i, 1);
                            break;
                        }
                    }
                    university.save();
                    company.save();
                    res.redirect('/#/uczelnie/' + university.id);
                }
            });
        }
    });
});

module.exports = router;
