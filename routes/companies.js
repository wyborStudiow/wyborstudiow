var express = require("express");
var router  = express.Router();
var University = require("../models/university");
var Company     = require("../models/company");
var middleware = require("../middleware");
var request = require("request");

//INDEX - show all companies
router.get("/", function(req, res){
    console.log("SZUKAM WSZYSTKICH FIRM");
    Company.find({}).populate("universities.item").exec(function(err, allCompanies) {
        if(err) {
            console.log("error");
        }
        else {
            res.send(allCompanies);
        }
    });
});

//CREATE - add new company to DB
router.post("/", middleware.isLoggedIn, function(req, res){
    console.log(req.body);
    var name = req.body.name;
    var image = req.body.image;
    var description = req.body.description;
    var newCompany = {name: name, img: image, description: description};
    Company.create(newCompany, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            res.redirect("/#/firmy");
        }
    });
});

// SHOW - shows more info about one company
router.get("/:id", function(req, res){
    console.log("SZUKAM KONKRETNEJ FIRMY");
    Company.findById(req.params.id).populate("universities.item").exec(function(err, foundCompany){
        if(err){
            console.log(err);
        } else {
            res.send({foundCompany: foundCompany, currentUser: res.locals.currentUser});
        }
    });
});

//EDIT - show edit form to one company
router.get("/:id/edit", middleware.isLoggedIn, function(req, res){
    console.log("IN EDIT!");
    Company.findById(req.params.id, function(err, foundCompany){
        if(err){
            console.log(err);
        } else {
            res.send(foundCompany);
        }
    });
});

//EDIT - put edited info about one company
router.put("/:id", middleware.isLoggedIn, function(req, res){
    var newData = {name: req.body.name, img: req.body.image, description: req.body.description};
    console.log(newData);
    console.log(req.params.id);
    Company.findByIdAndUpdate(req.params.id, {$set: newData}, function(err, company){
        if(err){
            console.log(err);
            res.redirect("back");
        } else {
            res.redirect("/#/firmy/" + company.id);
        }
    });
});

router.delete("/:companyId", middleware.isLoggedIn, function(req, res){
    Company.findById(req.params.companyId).populate("universities.item").exec(function(err, company) {
        if(err) {
            console.log(err);
            res.redirect("/#/firmy");
        } else {
            var numOfUniversities = company.universities.length;
            console.log(numOfUniversities + " na zewntarz ");
            if(numOfUniversities === 0) {
                deleteCompany(res, company);
            } else {
                company.universities.forEach(function(university) {
                   University.findById(university.item.id).populate("companies.item").exec(function(err, foundUniversity) {
                       if(err) {
                           console.log("nie znalazlem uniwersytetu w firmie");
                       } else {
                           for(var i = 0; i < foundUniversity.companies.length; i++) {
                               console.log(i + " iteracja");
                               console.log(foundUniversity);
                               console.log(foundUniversity.companies[i].item.id === req.params.companyId);
                                if(foundUniversity.companies[i].item.id === req.params.companyId) {
                                    foundUniversity.companies.splice(i, 1);
                                    break;
                                }
                            }
                            foundUniversity.save(function() {
                                numOfUniversities = numOfUniversities - 1;
                                console.log(numOfUniversities);
                                if(numOfUniversities === 0) {
                                    deleteCompany(res, company);
                                }
                            });
                       }
                   });
                });
            }
        }
    });
});

module.exports = router;

var deleteCompany = function(res, company) {
    Company.findByIdAndRemove(company.id, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("usunalem company");
            res.redirect("/#/firmy");
        }
    });
};
