require('dotenv').config()

var express = require("express");
var app = express();
var request = require("request");
const fs = require('fs');
const path = require('path');
var mongoose = require("mongoose");
var bodyParser  = require("body-parser"),
    methodOverride = require("method-override"),
    passport    = require("passport"),
    LocalStrategy = require("passport-local"),
    //flash        = require("connect-flash"),
    User        = require("./models/user");

let indexRoutes = require("./routes/index");
var uczelnieRoutes      = require("./routes/universities");
var firmyRoutes      = require("./routes/companies");
let wspolpracaRoutes = require("./routes/cooperation");

app.use('/dist', express.static(path.join(__dirname + "/dist")));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));

var url = process.env.DATABASEURL || "mongodb://localhost/studia_app";
mongoose.connect(url, {
    useMongoClient: true,
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Udalo sie polaczyc z baza danych");
});

if (process.env.NODE_ENV === 'development') {
    console.log("Env ustawione na development");
    require('./webpack-dev-middleware').init(app);
}

if (process.env.NODE_ENV === 'production') {
    console.log("Env ustawione na production");
    app.use('/dist', express.static(path.join(__dirname, 'dist')));
}


// PASSPORT CONFIGURATION
app.use(require("express-session")({
    secret: "Once again Fido wins cutest dog!",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next){
   res.locals.currentUser = req.user;
   //res.locals.success = req.flash('success');
   //res.locals.error = req.flash('error');
   next();
});

app.use("/", indexRoutes);
app.use("/uczelnie", uczelnieRoutes);
app.use("/firmy", firmyRoutes);
app.use("/wspolpraca", wspolpracaRoutes);

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("App has started!!!");
});

/*przykladowy_komentarz*/
